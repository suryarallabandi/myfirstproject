#jboss/wildfly
FROM suryarall/devopsprojectone
ADD samplewar/target/samplewar.war /opt/jboss/wildfly/standalone/deployments/
RUN /opt/jboss/wildfly/bin/add-user.sh admin admin --silent
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]

#Create Dockerfile with following content:
 #FROM jboss/wildfly
 #ADD your-awesome-app.war /opt/jboss/wildfly/standalone/deployments/
#Place your your-awesome-app.war file in the same directory as your Dockerfile

#To be able to create a management user to access the administration console create a Dockerfile with the following content
#FROM jboss/wildfly
#RUN /opt/jboss/wildfly/bin/add-user.sh admin Admin#70365 --silent
#CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]

