<!DOCTYPE html>
<html lang="en">
    <head>
        <title>First Devops Project</title>
        <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
        <style type="text/css">
            html{
                background: #EBEDEF;
                font-family: 'Ubuntu', sans-serif;
            }
            .header{
                width: 90%;
                margin: 0 auto;
            }   
            .head{
                float:left;
            }
            .nav{
                margin-top: 10px;
                float:right;
            }
            ul{
                list-style-type: none;
            }
            li{
                display: inline;
            }
            li a{
                color: black;
                padding: 20px;
                text-decoration: none;
            }
            li a:hover{
                color: #566573;
            }
            .container{
                width: 80%;
                margin:0 auto;
            }
            .login{
                text-align: center;
            }
            form{
                width: 800px;
                margin: 0 auto;
                margin-top: 70px;
            }
            input{
                border: none;
                outline: none;
                padding: 5px;
                width: 380px;
                height: 45px;
                font-family: 'Ubuntu', sans-serif;
            }
            input[type=submit]{
                background: #5DADE2;
                width: 785px;
                color: white;
            }
            select,option{
                border: none;
                outline: none;
                padding: 5px;
                width: 785px;
                height: 45px;
                font-family: 'Ubuntu', sans-serif;   
            }
            .copyright{
                margin-top: 50px;
                text-align: center;
            }
            span{
                font-size: 13px;
                letter-spacing: 1.5px;
            }
        </style>
    </head>
    <body>
        <div class="header">
            <div class="head">
                <h2>My DevOps Project</h2>
            </div>
            <div class="nav">
                <nav>
                    <ul>
                        <li><a href="#">Git</a></li>
                        <li><a href="#">Jenkins</a></li>
                        <li><a href="#">Docker</a></li>
                        <li><a href="#">Kubernetes</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div style="clear:both"></div>
        <div class="container">
            <div class="login">
                <form>
                    <input type="name" id="fname" name="fname" placeholder="First Name">
                    <input type="name" id="lname" name="lname" placeholder="Last Name">
                    <input type="phone" id="phone" name="phone" placeholder="Phone Number">
                    <input type="age" id="age" name="age" placeholder="Age">
                    <select id="prefix">
                        <option>----- Highest Qualification -----</option>
                        <option>Diploma</option>
                        <option>Bachelor's Degree</option>
                        <option>Master's Degree</option>
                        <option>Phd/Doctorate</option>
                    </select>
                    <input type="experience" id="exp" name="exp" placeholder="Prior Organization">
                    <input type="years" id="yrs" name="yrs" placeholder="No Of Years">
                    <input type="submit" name="submit" value="Register">
                </form>
            </div>
        </div>
        <div style="clear:both"></div>
        <div class="footer">
            <div class="copyright">
                <p>&copy; 2020 MyDevOpsProject. <span>Designed By Surya Rall</span></p>
            </div>
        </div>
    </body>
</html>
